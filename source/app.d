import devbuild.configuration;
import devbuild.windows;
import gdk.Threads;
import gio.Application: GioApplication = Application;
import gtkc.glibtypes;
import gtk.Application: Application;
import gtkc.giotypes;
import std.concurrency;

MainWindow win;

extern(C) static int hidle(void* data) {
	import std.file : dirEntries, SpanMode;
	import std.string : lastIndexOf;

	foreach (file; dirEntries("D:\\Projetos\\DevMaster\\", "*.csproj",  SpanMode.depth)) {
		win.addData(file[file.lastIndexOf("\\") + 1.. $]);
	}
	return 0;
}

void main(string[] args) {
	auto application = new Application("gtk.pwm", GApplicationFlags.FLAGS_NONE);
	application.addOnActivate(delegate void(GioApplication app){ 
			win = new MainWindow(application);
			gdk.Threads.threadsAddIdleFull(cast(int) GPriority.HIGH, &hidle, cast(void*) win, null);
			win.showAll();
		});
	application.run(args);
}
