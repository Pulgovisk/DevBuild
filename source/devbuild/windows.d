module devbuild.windows;

import gtk.ApplicationWindow : ApplicationWindow;
import gtk.Application : Application;
import gtk.Box;
import gtk.CellRendererText;
import gtk.ListStore;
import gtk.ScrolledWindow;
import gtk.TreeIter;
import gtk.TreeView;
import gtk.TreeViewColumn;

/**
 * This class is the DevBuild main application
 */
class MainWindow : ApplicationWindow {
	this(Application app) {
		super(app);

		setTitle("DevBuild");
		initUi();
	}

	void addData(string data) {
		TreeIter iter = store.createIter();
		store.setValue(iter, 0, data);
	}

private:
	void initUi() @trusted {
		setSizeRequest(1024,640);

		Box box = new Box(Orientation.VERTICAL,5);

		ScrolledWindow scroll = new ScrolledWindow();

		results = new TreeView();
		results.setHexpand(true);
		results.setVexpand(true);
		results.setHoverSelection(false);
		results.getSelection().setMode(GtkSelectionMode .MULTIPLE);

		results.appendColumn(new TreeViewColumn(
				"Projects", new CellRendererText(), "text", 0));

		store = new ListStore([GType.STRING]);
		results.setModel(store);

		scroll.add(results);
		box.add(scroll);
		add(box);
	}

	TreeView results;
	ListStore store;
}
