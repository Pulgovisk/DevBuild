module devbuild.configuration;

import std.file;
import std.json;

private const enum DEFAULT_CONFIGURATION_FILE = "DevBuild.json";

/**
 * The DevBuild configuration struct
 *
 * This struct store all DevBuild configurations
 * and represent in code the JSON configuration file
 */
struct Configuration {
	/**
	 * Initialize a new instance of this struct
	 */
	static Configuration opCall() {
		return Configuration.init;
	}

	/**
	 * Initialize a new instance of this class
	 *
	 * Params:
	 	val = The base JSON for initialize this struct
		 The JSON MUST contain the keys 'compilerCommand', 'projectRoot' and 'projectMatch'
	 */
	static Configuration opCall(in JSONValue val) {
		Configuration cfg = Configuration();
		cfg.compilerCommand = val["compilerCommand"].str;
		cfg.projectRoot = val["projectRoot"].str;
		cfg.projectMatch = val["projectMatch"].str;
		return cfg;
	}

	/**
	 * The DevBuild compiler command
	 *
	 * This is the command used by DevBuild for call the compiler
	 * This command can contain the name of propery of this struct inside the '{' and '}'
	 * and the DevBuild will replace the flag with de respective value in this struct instance
	 */
	string compilerCommand;
	/**
	 * The project root path
	 *
	 * This will be used by DevBuild for get all projects inside the root path
	 * Based on property $(BLUE Configration.projectMatch)
	 */
	string projectRoot;
	/**
	 * The match to use for find the project files
	 *
	 * This will be used for find all projects in the path specified by $(BLUE Configration.projectRoot)
	 */
	string projectMatch;
}

/**
 *
 */
Configuration loadConfigurationFromFile(in string fileName = DEFAULT_CONFIGURATION_FILE) {
	JSONValue val = parseJSON(readText(fileName));
	Configuration cfg = Configuration(val);
	val.destroy;
	return cfg;
}